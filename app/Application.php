<?php namespace App;

/**
 * Created by androide_osorio.
 * Date: 6/2/15
 * Time: 10:48
 */

use Laravel\Lumen\Application as BaseApplication;

class Application extends BaseApplication {

    /**
     * base url when lumen is in a subfolder
     */
    private $base_url = '/nos-pasa-a-todas/suscribete/index.php';

    /**
     * Get the current HTTP path info.
     *
     * @return string
     */
    public function getPathInfo()
    {
        $route_url = $this->removeTrailingSlashes($this->getRequestedroute());
        $route_url_no_params = $this->removeQueryStringFrom($route_url);

        $route = "/" . $this->removeTrailingSlashes($route_url_no_params, 'left');
        return $route;
    }

    /**
     * get the requested route by eliminating the subfolder/application base path
     * @return mixed
     */
    protected function getRequestedRoute()
    {
        return str_replace($this->base_url, '', $_SERVER['PHP_SELF']);
    }

    /**
     * remove query strings from route
     * @param $path
     *
     * @return mixed
     */
    protected function removeQueryStringFrom($path)
    {
        $query = isset( $_SERVER[ 'QUERY_STRING' ] ) ? $_SERVER[ 'QUERY_STRING' ] : '';
        return str_replace('?'.$query, '', $path);
    }

    /**
     * remove trailing slashes
     *
     * @param        $str
     * @param string $places
     *
     * @return string
     */
    protected function removeTrailingSlashes($str, $places = 'right')
    {
        if($places === 'left') {
            return ltrim($str, '/');
        }
        return rtrim($str, '/');
    }
}