<?php namespace App\Repositories;

/**
 * Created by androide_osorio.
 * Date: 5/29/15
 * Time: 12:04
 */

use DB;

class UsersRepository {

    private $table;

    public function __construct()
    {
        $this->table = 'users';
    }

    public function create($info)
    {
        return DB::table($this->table)->insertGetId($info);
    }

    public function count()
    {
        return DB::table($this->table)->count();
    }
}