<?php namespace App\Utilities\SOAP;

/**
 * Created by androide_osorio.
 * Date: 5/29/15
 * Time: 14:23
 */

use Artisaninweb\SoapWrapper\Extension\SoapService;

class PlusoftSoapClient extends SoapService {

    /**
     * @var string
     */
    protected $name = 'plusoft';

    /**
     * @var string
     */
    protected $wsdl;

    /**
     * @var boolean
     */
    protected $trace = true;

    //------------------------------------------------

    public function __construct()
    {
        $this->wsdl = env('SOAP_ADDRESS','http://10.57.32.226/csiweb-jnj/services/JnjWS?wsdl');
    }

    /**
     * Get all the available functions
     *
     * @return mixed
     */
    public function functions()
    {
        return $this->getFunctions();
    }

}