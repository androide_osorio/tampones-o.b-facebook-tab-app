<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * renders user subscription form
 */
$app->get( '/', [
    'uses' => 'App\Http\Controllers\UsersController@create',
    'as'   => 'users.create'
] );

/**
 * route for "creating" new users
 */
$app->post( '/users/new', [
    'uses' => 'App\Http\Controllers\UsersController@store',
    'as'   => 'users.store'
] );

/**
 * route for showing a message to a new registered user
 */
$app->get( '/enterate-de-nuestras-actividades-y-promociones', [
    'uses' => 'App\Http\Controllers\UsersController@welcome',
    'as'   => 'users.welcome'
] );

/**
 * route for displaying terms and conditions
 */
$app->get( '/terminos-y-condiciones', [
    'uses' => 'App\Http\Controllers\LegalsController@terms',
    'as'   => 'legals.terms-and-conditions'
] );

/**
 * test view to see if app is not broken
 */
$app->get( '/lumen', function () use ($app) {
    return $app->welcome();
} );