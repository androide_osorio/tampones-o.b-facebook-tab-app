<?php namespace App\Http\Controllers;

/**
 * Created by androide_osorio.
 * Date: 6/2/15
 * Time: 08:51
 */

use Laravel\Lumen\Routing\Controller as BaseController;

class LegalsController extends BaseController {

    /**
     * render terms and conditions
     * @return \Illuminate\View\View
     */
    public function terms()
    {
        return view('legals.terms-and-conditions');
    }
}