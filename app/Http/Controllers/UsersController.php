<?php namespace App\Http\Controllers;

/**
 * Created by androide_osorio.
 * Date: 5/28/15
 * Time: 16:10
 */

use App\Events\NewUserSubscribed;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class UsersController extends BaseController {

    /**
     * validation rules
     *
     * @var array
     */
    private $validation_rules = [
        'name'          => 'required|min:6',
        'email'         => 'required|email|unique:users|confirmed',
        'document_type' => 'required|in:cc,ti,cex,pass',
        'document_id'   => 'required|unique:users|min:6',
        'gender'        => 'required|in:F,M,O',
        'comment'       => 'required|min:10',
        'legals_tnc'    => 'required'
    ];

    /**
     * custom validation messages
     *
     * @var
     */
    private $error_messages = [
        'name.required'          => 'Debes ingresar tu nombre completo.',
        'name.min'               => 'Debes ingresar tu nombre completo.',
        'email.required'         => 'Debes ingresar tu email.',
        'email.email'            => 'Ingresa un e-mail valido (ejemplo:tu@ejemplo.com).',
        'email.unique'           => 'Ya estas registrado(a) en nuestra lista',
        'email.confirmed'        => 'La confirmación de tu e-mail no concuerda.',
        'document_type.required' => 'Debes ingresar tu tipo de documento de identidad.',
        'document_id.required'   => 'Debes ingresar tu documento de identidad.',
        'document_id.min'        => 'Tu documento de identidad debe tener más de 6 caracteres.',
        'document_id.unique'     => 'Ya hay una persona registrada con ese documento',
        'gender.required'        => 'Debes ingresar tu género.',
        'gender.in'              => 'No seleccionaste un género válido',
        'comment.required'       => 'Debes tener algo qué contarnos ¿no?',
        'comment.min'            => 'Seguramente tu inquietud o historia debe tener más de 10 caracteres',
        'legals_tnc.required'    => 'Debes aceptar los términos y condiciones para inscribirte.'
    ];

    public function __construct()
    {
        $this->users = new UsersRepository();
    }

    /**
     * Renders the registration form
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view( 'users.create' );
    }

    /**
     * "creates" a new user or returns validation errors
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate( $request, $this->validation_rules, $this->error_messages );

        // send email to SAC
        event( new NewUserSubscribed( $request->except('email_confirmation', 'legals_tnc', '_token') ) );

        return redirect()->route( 'users.welcome' )
            ->with('users_count', $this->users->count())
            ->with( 'message', 'Gracias por Registrarte!' );
    }

    /**
     * renders the welcome (users index) page
     */
    public function welcome()
    {
        return view( 'users.index' )->with('users_count', $this->users->count());
    }
}