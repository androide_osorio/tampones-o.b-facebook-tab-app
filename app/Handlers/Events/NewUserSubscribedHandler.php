<?php namespace App\Handlers\Events;

/**
 * Created by androide_osorio.
 * Date: 5/28/15
 * Time: 16:23
 */
use App\Repositories\UsersRepository;
use Carbon\Carbon;
use \Mail;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Mail\Message;
use App\Events\NewUserSubscribed;
use App\Utilities\SOAP\PlusoftSoapclient;

class NewUserSubscribedHandler {

    /**
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->users = new UsersRepository();
    }
    /**
     * handles the NewUserSubscribed Event
     * @param $event
     */
    public function handle(NewUserSubscribed $event)
    {
        $user_info = $event->getUserInfo();

        //$this->sendToPlusoft();

        //register user
        $this->saveUser( $user_info );

        //send email to SAC
        $this->sendNotificationEmail($user_info);
    }

    /**
     * send a notification email to SAC about the new user
     * @param $user_info
     */
    protected function sendNotificationEmail($user_info)
    {
        $this->mailer->send('emails.users.subscription', $user_info, function(Message $message)
        {
            $message->to('andresosorio@owak.co', 'SAC')
                    ->from('noreply@viveob.com', 'Nos pasa a Todas en Facebook')
                    ->subject('Nuevo usuario inscrito a través de facebook');
        });
    }

    /**
     * this method uses PHPs SOAP client for registering a new user in jnjcolombia.com
     */
    protected function sendToPlusoft()
    {
        //TODO: implement SOAP client
        $soapClient = new PlusoftSoapClient();
        dd($soapClient->functions());
    }

    /**
     * saves a user into the database
     * @param $user_info
     */
    protected function saveUser($user_info)
    {
        $this->users->create( $user_info );
    }
}