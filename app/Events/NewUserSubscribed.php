<?php namespace App\Events;

/**
 * Created by androide_osorio.
 * Date: 5/28/15
 * Time: 16:24
 */

use Carbon\Carbon;

class NewUserSubscribed {

    /**
     * @var array
     */
    private $user_info;

    /**
     * constructor
     *
     * @param array $user_info
     */
    public function __construct(array $user_info)
    {
        $this->user_info = $user_info;
        $this->getDOB();
        $this->timestamps();
        $this->user_info['gender'] = $this->transformGender($this->user_info['gender']);
    }

    public function getUserInfo()
    {
        return $this->user_info;
    }

    /**
     * return true if the date is fragmented (using the keys year, month and day)
     * @return bool
     */
    protected function isDateFragmented()
    {
        $fields             = array_keys( $this->user_info );
        $fragmentedDateKeys = [ 'year', 'month', 'day' ];
        $dateFragmented     = array_intersect( $fields, $fragmentedDateKeys );

        return !empty( $dateFragmented );
    }

    /**
     * return the date of birth in the correct format
     * @return bool|string
     */
    protected function getDOB()
    {
        if($this->isDateFragmented()) {
            $date = Carbon::createFromDate(
                $this->user_info[ 'year' ],
                $this->user_info[ 'month' ],
                $this->user_info[ 'day' ]
            );

            $this->user_info['dob'] = $date;
            unset($this->user_info[ 'year' ], $this->user_info[ 'month' ], $this->user_info[ 'day' ]);
        }
        return $this->user_info['dob'];
    }

    /**
     * transforms a gender identifier into a full word
     * @param $genderIdentifier
     *
     * @return string
     */
    protected function transformGender($genderIdentifier)
    {
        switch($genderIdentifier) {
            case 'M': return 'Masculino';
            break;
            case 'F': return 'Femenino';
            break;
            default: return 'Otro';
            break;
        }
    }

    /**
     * adds timestamps to the user info
     */
    protected function timestamps()
    {
        $this->user_info['created_at'] = Carbon::now();
        $this->user_info['updated_at'] = Carbon::now();
    }
}