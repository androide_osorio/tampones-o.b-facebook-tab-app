<?php namespace App\Providers;

/**
 * Created by androide_osorio.
 * Date: 5/28/15
 * Time: 16:19
 */

use Illuminate\Support\ServiceProvider;

class EventsServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['events']->listen(
            'App\Events\NewUserSubscribed', 'App\Handlers\Events\NewUserSubscribedHandler@handle'
        );
    }
}