/**
 * Assets Tasks file with Gulp
 * Uses Gulp as build system. used mainly for watching changes
 * on the assets and process them to then copy the results into the public directory
 *
 * Created by androide_osorio on 5/27/15.
 */

//require gulp and gulp-load-plugins. We then call the gulp-load-plugins task
//to automatically load all installed gulp plugins
var gulp   = require("gulp"),
$          = require("gulp-load-plugins")(),
bowerFiles = require('main-bower-files');

//PATHS
var directories = {
    public_dir: '../../public/',
    styles    : { src: 'scss'  , dist: 'styles/' },
    scripts   : { src: 'js', dist: 'scripts/' },
    images    : { src: 'images', dist: 'images/' },
    fonts     : { src: 'fonts', dist: 'fonts/' },
    bower     : { src: 'bower_components', dist: 'vendor/' },
    templates : { src: '../views' },
    fonts     : { dist: 'fonts/' }
}, dirs = directories;

var get_dist_dir = function(target) {
    var root_dir = directories.public_dir;
    return root_dir + dirs[target].dist;
};

//------------------------------------------------------------------------------------
/* ------------------------------------- *
 * Main Styles Tasks
 * ------------------------------------- */
/* Compile Our Sass */
gulp.task('sass', function() {
    var dest_dir = get_dist_dir('styles');

    return gulp.src(dirs.styles.src + '/**/*.scss')
        .pipe( $.sass())
        .pipe(gulp.dest(dest_dir))
        .pipe( $.rename({suffix: '.min'}))
        .pipe( $.minifyCss())
        .pipe(gulp.dest(dest_dir))
        .pipe( $.livereload({ start: true }) );
});

gulp.task('js', function() {
    var dest_dir = get_dist_dir('scripts');

    return gulp.src(dirs.scripts.src + '/**/*.js')
        .pipe( $.jshint() )
        .pipe(gulp.dest(dest_dir))
        .pipe( $.rename({suffix: '.min'}))
        .pipe( $.uglify())
        .pipe(gulp.dest(dest_dir))
        .pipe( $.livereload({ start: true }) );
});

gulp.task('blade', function() {
    return gulp.src(dirs.templates.src + '/**/*.blade.php')
        .pipe( $.livereload({ start: true }) );
});
//---------------------------------------------------------------------------
/* ------------------------------------- *
 * Other Assets tasks
 * ------------------------------------- */
/* Bower Styles concatenation */
gulp.task('bower:css', function() {
    var distDir = get_dist_dir( 'styles' );

    return gulp.src( bowerFiles())
        .pipe($.filter('**/*.css'))
        .pipe($.concat('vendor.css'))
        .pipe(gulp.dest( distDir ))
        .pipe( $.rename({suffix: '.min'}))
        .pipe($.minifyCss())
        .pipe( $.plumber() )
        .pipe(gulp.dest( distDir ))
        .pipe( $.livereload({ start: true }) )
});

/* Bower libraries concatenation */
gulp.task('bower:js', function () {
    var distDir = get_dist_dir( 'scripts' );

    return gulp.src(bowerFiles())
        .pipe($.filter(['**/*.js', '!**/*.min.js', '!jquery.js', '!jquery.min.js']))
        .pipe($.concat('vendor.js'))
        .pipe(gulp.dest( distDir ))
        .pipe( $.rename({suffix: '.min'}))
        .pipe($.uglify())
        .pipe(gulp.dest(distDir))
});

/* Bower font assets */
/* Optimize and cop fonts */
gulp.task('bower:fonts', function () {
    var distDir = get_dist_dir( 'fonts' );
    return gulp.src(bowerFiles())
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe($.flatten())
        .pipe( $.plumber() )
        .pipe(gulp.dest( distDir ));
});

 //---------------------------------------------------------------------------
/* ------------------------------------- *
 * Watchers
 * ------------------------------------- */

 /* Watch Files For Changes */
gulp.task('watch', function() {
    $.livereload.listen();
    gulp.watch(dirs.templates.src + '/**/*.blade.php', ['blade']);
    gulp.watch(dirs.styles.src + '/**/*.scss', ['sass']);
    gulp.watch(dirs.scripts.src + '/**/*.js', ['js']);
    gulp.watch(dirs.bower.src + '/**/*.js', ['bower:js']);
    gulp.watch(dirs.bower.src + '/**/*.{eot,svg,ttf,woff}', ['bower:fonts']);

    /* Trigger a live reload on any Django template changes */
    gulp.watch(dirs.templates + '/*.blade.php').on('change', $.livereload.changed);

});

//---------------------------------------------------------------------------
/* ------------------------------------- *
 * Main tasks
 * ------------------------------------- */

/* Default Task */
gulp.task('default', [
    'sass',
    'js',
    //'bower:css',
    'bower:js',
    'bower:fonts',
    'watch'
]);