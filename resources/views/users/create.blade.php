@extends('master')

@section('title') inscribete @stop

@section('content')
    <div class="column small-11 medium-8 small-centered email-form-panel">
        <h2>
            <strong style="font-style: italic;">Inscr&iacute;bete</strong> y haz parte<br>
            de <strong>todo</strong> lo que<br>
            <strong>tenemos para ti</strong>
        </h2>
        <p class="emphasized-text">Si a&uacute;n no te has inscrito a nuestro Email marketing<br>
            completa el cuestionario y entérate de lo &uacute;ltimo.</p>

        @include('elements.subscription-form')
    </div>
@stop