@extends('master')

@section('title') Únete a las {{ $users_count }} inscritas @stop

@section('content')
    <div class="column small-12 medium-8 small-centered text-center" id="users-count-container">
        <hgroup>
            <h2 id="users-count">{{ $users_count }}</h2>
            <img src="{{ url('public/images/icon-email-marketing.svg') }}" alt="inscritas" class="email-marketing-icon" />
            <h4 class="subtle-header">Inscritas</h4>
        </hgroup>
        <br>
        <h3><strong>Entérate</strong> <em>de nuestras</em><br>
            <em>actividades y</em>  <strong>promociones</strong></h3>
        <hr />
        <h3 class="emphasis-header">
            Recuerda, solo las <br class="show-on-small"><strong class="header-important-word">200</strong> primeras<br>
            <small>inscritas serán ganadoras de unos increíbles panties</small>
        </h3>
    </div>
@stop