<!doctype html>
<html lang="es">
<head>
    <title>@yield('title') - Nos pasa a todas: Tampones o.b® y CAREFREE®</title>

    {{-- Meta tags --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    {{-- / Meta tags --}}

    {{-- Stylesheets --}}
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('public/styles/main.css') }}" />
    {{-- / Stylesheets --}}
</head>
<body>
    {{-- main container --}}
    <div class="row">
        {{-- Header --}}
        @include('elements.header')
        {{-- / Header --}}

        <div class="row">
            @yield('content')
        </div>

        {{-- Footer --}}
        <footer id="main-footer" class="row">
            @include('elements.footer')
        </footer>
        {{-- / Footer --}}
    </div>
    {{-- /main container --}}

    {{-- Scripts --}}
    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="{{ url('public/scripts/vendor.js') }}"></script>
    <script>
        (function($, window, document, undefined) {
            $(document ).foundation();
        })(window.jQuery, window, document);
    </script>
    {{-- / Scripts --}}
</body>
</html>