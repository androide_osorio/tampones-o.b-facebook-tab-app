{{-- Alert box --}}
@if(isset($errors) && !$errors->isEmpty())
    <div class="alert-box alert">
        Hay errores en la información que llenaste. Por favor, revisa cada campo.
        <ul>
            @foreach($errors->all() as $error)
                <li style="color: white;">{{ $error }}</li>
            @endforeach
        </ul>
        <a href="#" class="close">&times;</a>
    </div>
@endif

<form action="{{ route('users.store') }}" id="subscription-form" data-abide method="POST">
    {{-- User full name --}}
    <div class="row">
        <div class="large-12 columns name-field">
            <label>Ingresa aqu&iacute; tu Nombre Completo
                <input type="text" name="name" placeholder="Ingresa tu nombre completo" required pattern="[a-zA-Z\s]+" value="{{ old('name') }}" />
            </label>
            <small class="error">Debes ingresar tu nombre completo y sólo debe contener letras y espacios.</small>
        </div>
    </div>

    {{-- user e-mail --}}
    <div class="row">
        <div class="small-12 column email-field">
            <div class="row collapse prefix-radius">
                <div class="small-12 columns">
                    <label for="">Ingresa aqu&iacute; tu E-mail</label>
                </div>
                <div class="small-2 medium-1 columns">
                    <span class="prefix"><i class="fa fa-envelope-o"></i></span>
                </div>
                <div class="small-10 medium-11 columns">
                    <input type="email" name="email" placeholder="Ingresa tu e-mail" required value="{{ old('email') }}" />
                    <small class="error">Debes ingresar tu e-mail. Asegúrate que tiene un formato válido (tu@ejemplo.com)</small>
                </div>
            </div>
        </div>
    </div>

    {{-- user e-mail confirmation --}}
    <div class="row">
        <div class="small-12 column">
            <div class="row collapse prefix-radius">
                <div class="small-12 columns">
                    <label for="">Vuelve a Ingresar tu E-mail</label>
                </div>
                <div class="small-2 medium-1 columns">
                    <span class="prefix"><i class="fa fa-envelope-o"></i></span>
                </div>
                <div class="small-10 medium-11 columns">
                    <input type="email" name="email_confirmation" placeholder="Ingresa tu e-mail de nuevo" required data-equal-to="email" />
                    <small class="error">Los emails no concuerdan</small>
                </div>
            </div>
        </div>
    </div>

    {{-- user document ID --}}
    <fieldset class="row">
        <div class="small-12 columns">
            <label for="document-type-input">Ingresa tu Documento de Identidad:</label>
        </div>
        <div class="medium-6 small-12 columns">
            <select name="document_type" id="document-type-input" class="select-green" required value="{{ old('document_type') }}">
                <option value="">Tipo de documento</option>
                <option value="cc">Cédula de Ciudadanía</option>
                <option value="ti">Tarjeta de Identidad</option>
                <option value="cex">Cédula de extranjería</option>
                <option value="pass">Pasaporte</option>
            </select>
            <small class="error">Debes seleccionar un tipo de documento de identidad</small>

        </div>
        <div class="medium-6 small-12 columns">
            <input type="text" name="document_id" id="document-input" placeholder="Número de documento" required value="{{ old('document_id') }}" />
            <small class="error">Debes ingresar tu documento de identidad (mínimo 6 caracteres).</small>
        </div>
    </fieldset>

    {{-- Date of birth --}}
    <fieldset class="row">
        <div class="small-12 columns">
            <label>Ingresa tu fecha de nacimiento:</label>
        </div>

        <div id="desktop-picker" class="visible-for-medium-up">
            <div class="medium-4 small-12 columns">
                <select name="year" id="" class="select-green" required value="{{ old('year') }}">
                    <option value="">Año</option>
                    @for($i = 1900; $i <= date('Y'); $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
                <small class="error">Debes seleccionar tu año de nacimiento</small>
            </div>
            <div class="medium-4 small-12 columns">
                <select name="month" id="" class="select-green" required value="{{ old('month') }}">
                    <option value="">mes</option>
                    <option value="01">Enero</option>
                    <option value="02">Febrero</option>
                    <option value="03">Marzo</option>
                    <option value="04">Abril</option>
                    <option value="05">Mayo</option>
                    <option value="06">Junio</option>
                    <option value="07">Julio</option>
                    <option value="08">Agosto</option>
                    <option value="09">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                </select>
                <small class="error">Debes seleccionar tu mes de nacimiento</small>

            </div>
            <div class="medium-4 small-12 columns">
                <select name="day" id="" class="select-green" required value="{{ old('day') }}">
                    <option value="">día</option>
                    @for($i = 1; $i < 32; $i++)
                        <?php $numDay = sprintf("%02d", $i); ?>
                        <option value="{{ $numDay }}">{{ $numDay }}</option>
                    @endfor
                </select>
                <small class="error">Debes seleccionar tu día de nacimiento</small>
            </div>
        </div>
    </fieldset>
    {{-- / Date of birth --}}

    {{-- Gender --}}
    <fieldset class="row">
        <div class="small-12 columns">
            <label for="gender-select">Ingresa tu género:</label>
        </div>

        <div class="small-12 medium-5 columns end">
            <select name="gender" id="gender-select" class="select-green" required value="{{ old('gender') }}">
                <option value="">Género</option>
                <option value="F">Femenino</option>
                <option value="M">Masculino</option>
                <option value="O">Otro</option>
            </select>
            <small class="error">Debes seleccionar tu género</small>

        </div>
    </fieldset>

    {{-- Question or Story --}}
    <fieldset class="row">
        <div class="small-12 columns">
            <label>&iquest;Tienes una historia o inquietud para contarnos<br>
                relacionada con tu período? Comp&aacute;rtenosla aquí:</label>
        </div>

        <div class="small-12 columns">
            <textarea name="comment" id="" cols="30" rows="10" placeholder="Compartenos tu historia o inquietudes
relacionadas a tu período" required>value="{{ old('comment') }}"</textarea>
            <small class="error">Debes tener algo qué contarnos ¿no?</small>
        </div>
    </fieldset>

    {{-- Legal Opt-ins --}}
    <fieldset class="row">
        <small class="error">Debes aceptar los términos y condiciones para inscribirte</small>

        <div class="small-12 columns">
            <label for="terms-and-conditions" class="legals-label">
                <input id="terms-and-conditions" name="legals_tnc" type="checkbox" class="checkbox-custom" required>
                &iquest;Aceptas los términos y condiciones? <a href="{{ route('legals.terms-and-conditions') }}">Ver Términos</a>
            </label>
        </div>

        <div class="small-12 columns">
            <label for="privacy-policy" class="legals-label">
                <input id="privacy-policy" name="legals_privacy_policy" type="checkbox" class="checkbox-custom">
                &iquest;Aceptas que tu pregunta/historia sea publicada de
                forma anónima para ayudar a otras mujeres que
                tengan tu misma duda?
            </label>
        </div>

        <div class="small-12 columns">
            <label for="add-to-email-list" class="legals-label">
                <input id="add-to-email-list" name="legals_email_list" type="checkbox" class="checkbox-custom">
                &iquest;Aceptas que te contactemos via Email?
            </label>
        </div>
    </fieldset>

    <div class="row">
        <div class="small-9 columns small-centered">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" value="ENVIAR" class="button" />
        </div>
    </div>
</form>
